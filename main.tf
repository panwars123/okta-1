provider "okta" {
  org_name  = "dev-460564"
  base_url  = "okta.com"
  api_token = "00QLw9WDq6EDBmW9t9zF1uri818i3ejvx5n4HPNjnh"
}



# okta_app_oauth.mega:
resource "okta_app_oauth" "mega" {
    auto_key_rotation          = true
    auto_submit_toolbar        = false
    client_id                  = "0oa1hgcdab0lpGqdf4x7"
    consent_method             = "TRUSTED"
    grant_types                = [
        "authorization_code",
        "implicit",
    ]
    groups                     = [
        "00g1hg6qmwiiGuJhO4x7",
    ]
    hide_ios                   = true
    hide_web                   = true
#    id                         = "0oa1hgcdab0lpGqdf4x7"
    issuer_mode                = "ORG_URL"
    label                      = "testing-app"
    login_mode                 = "DISABLED"
    login_scopes               = []
#    name                       = "oidc_client"
    post_logout_redirect_uris  = [
        "http://dev.com",
        "http://fb.com",
        "http://hhh.com",
        "http://wa.com",
    ]
    redirect_uris              = [
        "http://azure",
        "http://d.com/",
        "http://gcp",
        "http://hcp",
        "http://jh.com",
        "http://ko.com",
        "http://kpmg.com",
        "http://prod.com",
        "http://uat.com",
        "http:do",
        "http:oracle",
    ]
    response_types             = [
        "code",
        "id_token",
        "token",
    ]
#    sign_on_mode               = "OPENID_CONNECT"
    status                     = "ACTIVE"
    token_endpoint_auth_method = "client_secret_basic"
    type                       = "web"

    users {
        id       = "00u1hgcda8bIZ9eJR4x7"
#        scope    = "USER"
        username = "John@example.com"
    }
    users {
        id       = "00u1mmhgr648CRdw54x7"
#        scope    = "USER"
        username = "lopi@gmail.com"
    }
    users {
        id       = "00u1p5hl9a73hEngQ4x7"
#        scope    = "USER"
        username = "pra@gmail.com"
    }
    users {
        id       = "00u1pd28vp9DqSq2P4x7"
#        scope    = "USER"
        username = "gcp@gmail.com"
    }
}
